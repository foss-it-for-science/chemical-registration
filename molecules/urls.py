from django.contrib import admin
from django.urls import path
from . import views

app_name = 'molecules'

urlpatterns = [
    path('', views.index),
]