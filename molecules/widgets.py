from django import forms

class MolecularEditor(forms.Textarea):
    template_name = 'molecules/ketcher/widgets/molecular_editor.html'
    class Media:
        js = [
            'core/jsme/jsme.nocache.js',
            'molecules/js/jsmeUtils.js',
        ]
    def __init__(self, attrs=None):
        final_attrs = {
            'class': 'vLargeTextField',
            'style': "height: 390px; width: 450px",
            'height': '400px',
            'width': '500px',
        }
        if attrs is not None:
            final_attrs.update(attrs)
        super().__init__(attrs=final_attrs)