from django.db import models

class Compound(models.Model):
    name = models.CharField(max_length=400, unique=True)
    ctab = models.TextField()
    class Meta:
        pass
    def __str__(self) -> str:
        return self.name

class Form(models.Model):
    name = models.CharField(max_length=400, unique=True)
    ctab = models.TextField()
    compound = models.ForeignKey(Compound, on_delete=models.CASCADE)
    class Meta:
        pass
    def __str__(self) -> str:
        return self.name

class Batch(models.Model):
    name = models.CharField(max_length=400, unique=True)
    form = models.ForeignKey(Form, on_delete=models.CASCADE)
    class Meta:
        pass
    def __str__(self) -> str:
        return self.name
