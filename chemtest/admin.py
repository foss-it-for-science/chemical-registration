from django.contrib import admin
from django.contrib.admin.options import ModelAdmin
from .models import Compound, Form, Batch

@admin.register(Compound)
class CompoundAdmin(admin.ModelAdmin):
    pass

@admin.register(Form)
class FormAdmin(admin.ModelAdmin):
    pass

@admin.register(Batch)
class BatchAdmin(admin.ModelAdmin):
    pass