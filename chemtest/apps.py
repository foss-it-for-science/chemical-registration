from django.apps import AppConfig


class ChemtestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'chemtest'
